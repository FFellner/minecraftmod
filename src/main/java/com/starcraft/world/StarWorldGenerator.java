package com.starcraft.world;

import com.starcraft.proxy.CommonProxy;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class StarWorldGenerator implements IWorldGenerator {

    private WorldGenerator titaniumGenerator;
    private WorldGenerator energyGenerator;
    private WorldGenerator crystalGenerator;

    public StarWorldGenerator() {
        this.titaniumGenerator = new WorldGenMinable(CommonProxy.TITANIUM_ORE.getDefaultState(),6);
        this.energyGenerator = new WorldGenMinable(CommonProxy.ENERGY_ORE.getDefaultState(),3);
        this.crystalGenerator = new WorldGenMinable(CommonProxy.CRYSTAL_ORE.getDefaultState(),1);
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        switch (world.provider.getDimension()) {
            case 0:
                this.runGenerator(titaniumGenerator, world, random, chunkX, chunkZ, 70, 0, 50);
                this.runGenerator(energyGenerator, world, random, chunkX, chunkZ, 50, 0, 50);
                this.runGenerator(crystalGenerator, world, random, chunkX, chunkZ, 30, 0, 50);
                break;
            case 1:
                break;
            case -1:
                break;
        }
    }

    private void runGenerator(WorldGenerator generator, World world, Random rand, int chunk_X, int chunk_Z, int chancesToSpawn, int minHeight, int maxHeight) {
        if (minHeight < 0 || maxHeight > 256 || minHeight > maxHeight) {
            throw new IllegalArgumentException("Illegal Height Arguments for WorldGenerator");
        }

        int heightDiff = maxHeight -minHeight + 1;
        for (int i = 0; i < chancesToSpawn; i++) {
            int x = chunk_X * 16 + rand.nextInt(16);
            int y = minHeight + rand.nextInt(heightDiff);
            int z = chunk_Z * 16 + rand.nextInt(16);
            generator.generate(world, rand, new BlockPos(x, y, z));
        }
    }
}
