package com.starcraft.proxy;

import com.starcraft.Starcraft;
import com.starcraft.blocks.CrystalOre;
import com.starcraft.blocks.EnergyOre;
import com.starcraft.blocks.Titanium;
import com.starcraft.entities.EntityBullet;
import com.starcraft.entities.RenderStormtrooper;
import com.starcraft.entities.Stormtrooper;
import com.starcraft.items.*;
import com.starcraft.world.StarWorldGenerator;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.init.Biomes;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod.EventBusSubscriber(modid = Starcraft.MODID)
public class CommonProxy {

    public static Block TITANIUM_ORE = new Titanium("titaniumore");
    public static Block ENERGY_ORE = new EnergyOre("energyore");
    public static Block CRYSTAL_ORE = new CrystalOre("crystalore");

    public static Item BATTERY = new Battery("battery");
    public static Item CRYSTAL = new Crystal("crystal");
    public static Item ENERGY_STONE = new EnergyStone("energystone");
    public static Item TITANIUM_INGOT = new TitaniumIngot("titaniumingot");

    public static Item LASER_SWORD = new LaserSword("lasersword");
    public static Item LASER_GUN = new LaserGun("lasergun");

    public static StarWorldGenerator generator = new StarWorldGenerator();

    public void preInit(FMLPreInitializationEvent event) {
        GameRegistry.registerWorldGenerator(generator, 0);
        registerEntity(EntityBullet.class, "bullet", 0);

        registerEntity(Stormtrooper.class, "stormtrooper", 1);
        EntityRegistry.addSpawn(Stormtrooper.class, 100, 3, 5, EnumCreatureType.MONSTER, Biomes.PLAINS, Biomes.DESERT, Biomes.ICE_PLAINS);
        RenderingRegistry.registerEntityRenderingHandler(Stormtrooper.class, RenderStormtrooper.FACTORY);
    }

    public void init(FMLInitializationEvent event) {
        GameRegistry.addSmelting(TITANIUM_ORE, new ItemStack(TITANIUM_INGOT), 0);
        GameRegistry.addSmelting(ENERGY_ORE, new ItemStack(ENERGY_STONE), 0);
        GameRegistry.addSmelting(CRYSTAL_ORE, new ItemStack(CRYSTAL), 0);
    }

    public void postInit(FMLPostInitializationEvent event) {

    }

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {
        event.getRegistry().registerAll(TITANIUM_ORE, ENERGY_ORE, CRYSTAL_ORE);
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(BATTERY, ENERGY_STONE, CRYSTAL, TITANIUM_INGOT, LASER_SWORD, LASER_GUN);
        event.getRegistry().register(new ItemBlock(TITANIUM_ORE).setRegistryName(TITANIUM_ORE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(ENERGY_ORE).setRegistryName(ENERGY_ORE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(CRYSTAL_ORE).setRegistryName(CRYSTAL_ORE.getRegistryName()));
    }

    @SubscribeEvent
    public static void registerRenderers(ModelRegistryEvent event) {
        registerRenderer(BATTERY);
        registerRenderer(ENERGY_STONE);
        registerRenderer(TITANIUM_INGOT);
        registerRenderer(LASER_GUN);
        registerRenderer(Item.getItemFromBlock(TITANIUM_ORE));
        registerRenderer(Item.getItemFromBlock(ENERGY_ORE));
        registerRenderer(Item.getItemFromBlock(CRYSTAL_ORE));
        registerItemVariants(CRYSTAL);
        registerItemVariants(LASER_SWORD);
    }

    public static void registerItemVariants(Item item) {
        ModelLoader.setCustomMeshDefinition(item, stack -> new ModelResourceLocation(stack.getItem().getRegistryName() + "_" + CrystalUtil.getRegistryNameFromNBT(stack), "inventory"));
        for(CrystalUtil.CrystalType type : CrystalUtil.CrystalType.values()) {
            ModelBakery.registerItemVariants(item, new ResourceLocation(item.getRegistryName().toString() + "_" + type.getName()));
        }
    }

    public static void registerEntity(Class entity, String name, int id) {
        EntityRegistry.registerModEntity(new ResourceLocation("starcraft", name), entity, name, 1337 + id, Starcraft.instance, 64, 1, true);
    }

    public static void registerRenderer(Item item) {
        ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(item.getRegistryName(), "inventory"));
    }
}
