package com.starcraft.entities;

import net.minecraft.entity.ai.EntityAIAttackMelee;

public class StormtrooperAttack extends EntityAIAttackMelee {

    private int raiseArmTicks;
    private Stormtrooper stormtrooper;

    public StormtrooperAttack(Stormtrooper stormtrooper, double speedIn, boolean longMemoryIn) {
        super(stormtrooper, speedIn, longMemoryIn);
        this.stormtrooper = stormtrooper;
    }

    @Override
    public void startExecuting() {
        super.startExecuting();
        this.raiseArmTicks = 0;
    }

    @Override
    public void resetTask() {
        super.resetTask();
        this.stormtrooper.setArmsRaised(false);
    }

    @Override
    public void updateTask() {
        super.updateTask();
        ++this.raiseArmTicks;

        if (this.raiseArmTicks >= 5 && this.attackTick < 10) {
            this.stormtrooper.setArmsRaised(true);
        } else {
            this.stormtrooper.setArmsRaised(false);
        }
    }
}
