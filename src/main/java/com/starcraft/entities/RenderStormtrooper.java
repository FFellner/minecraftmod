package com.starcraft.entities;

import net.minecraft.client.model.ModelZombie;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.IRenderFactory;

import javax.annotation.Nonnull;

public class RenderStormtrooper extends RenderLiving<Stormtrooper> {

    private ResourceLocation mobTexture = new ResourceLocation("starcraft:textures/entity/stormtrooper.png");

    public static final Factory FACTORY = new Factory();

    public RenderStormtrooper(RenderManager rendermanagerIn) {
        super(rendermanagerIn, new ModelZombie(), 0.5F);
    }

    @Override
    @Nonnull
    protected ResourceLocation getEntityTexture(@Nonnull Stormtrooper entity) {
        return mobTexture;
    }

    public static class Factory implements IRenderFactory<Stormtrooper> {

        @Override
        public Render<? super Stormtrooper> createRenderFor(RenderManager manager) {
            return new RenderStormtrooper(manager);
        }

    }

}
