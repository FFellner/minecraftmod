package com.starcraft;

import com.starcraft.proxy.CommonProxy;
import com.starcraft.world.StarWorldGenerator;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = Starcraft.MODID, name = Starcraft.NAME, version = Starcraft.VERSION)
public class Starcraft
{
    public static final String MODID = "starcraft";
    public static final String NAME = "Starcraft Mod";
    public static final String VERSION = "1.0";

    @Mod.Instance
    public static Starcraft instance;

    @SidedProxy(clientSide = "com.starcraft.proxy.ClientProxy", serverSide = "com.starcraft.proxy.ServerProxy")
    public static CommonProxy proxy;

    public static final CreativeTabs creativeTab = (new CreativeTabs("creativeTab") {
        @Override
        public ItemStack getTabIconItem() {
            return new ItemStack(CommonProxy.LASER_SWORD);
        }
    });

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        System.out.println("preInit");
        StarWorldGenerator generator = new StarWorldGenerator();
        GameRegistry.registerWorldGenerator(generator, 0);
        proxy.preInit(event);
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        System.out.println("init");
        proxy.init(event);
    }

    @EventHandler
    public void init(FMLPostInitializationEvent event)
    {
        System.out.println("postInit");
        proxy.postInit(event);
    }
}
