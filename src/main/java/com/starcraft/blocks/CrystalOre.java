package com.starcraft.blocks;

import com.starcraft.Starcraft;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class CrystalOre extends Block {

    public CrystalOre(String name) {
        super(Material.ROCK);
        setUnlocalizedName(name);
        setRegistryName(name);
        setCreativeTab(Starcraft.creativeTab);
        setSoundType(SoundType.STONE);
    }
}