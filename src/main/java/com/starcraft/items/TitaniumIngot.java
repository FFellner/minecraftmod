package com.starcraft.items;

import com.starcraft.Starcraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class TitaniumIngot extends Item {

    public TitaniumIngot(String name) {
        setUnlocalizedName(name);
        setRegistryName(name);
        setCreativeTab(Starcraft.creativeTab);
    }

}
