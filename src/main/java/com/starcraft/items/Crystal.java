package com.starcraft.items;

import com.starcraft.Starcraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class Crystal extends Item {

    public Crystal(String name) {
        setUnlocalizedName(name);
        setRegistryName(name);
        setCreativeTab(Starcraft.creativeTab);
        setHasSubtypes(true);
    }

    @Override
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
        if(this.isInCreativeTab(tab)) {
            for(CrystalUtil.CrystalType type : CrystalUtil.CrystalType.values())
                items.add(CrystalUtil.create(new ItemStack(this), type));
        }
    }

    @Override
    public String getUnlocalizedName(ItemStack stack) {
        return super.getUnlocalizedName(stack) + "." + CrystalUtil.getRegistryNameFromNBT(stack);
    }

}
