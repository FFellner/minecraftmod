package com.starcraft.items;

import com.starcraft.Starcraft;
import com.starcraft.entities.EntityBullet;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.*;
import net.minecraft.util.*;
import net.minecraft.world.World;

public class LaserGun extends Item {

    public LaserGun(String name) {
        maxStackSize = 1;
        setCreativeTab(Starcraft.creativeTab);
        setUnlocalizedName(name);
        setRegistryName(name);
        setMaxDamage(10);
    }

    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
    {
        ItemStack itemstack = playerIn.getHeldItem(handIn);
        ItemStack ammo = findAmmo(playerIn);

        if (itemstack.getMaxDamage() - itemstack.getItemDamage() > 1) {
            //worldIn.playSound((EntityPlayer)null, playerIn.posX, playerIn.posY, playerIn.posZ, SoundEvents.ENTITY_SNOWBALL_THROW, SoundCategory.NEUTRAL, 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

            EntityBullet bullet = new EntityBullet(worldIn, playerIn);
            bullet.shoot(playerIn, playerIn.rotationPitch, playerIn.rotationYaw, 0.0F, 5.0F, 0.1F);
            worldIn.spawnEntity(bullet);

            itemstack.damageItem(1, playerIn);

            return new ActionResult<>(EnumActionResult.SUCCESS, itemstack);
        } else if (!ammo.isEmpty()) {
            itemstack.setItemDamage(ammo.getItemDamage());
            ammo.setItemDamage(ammo.getMaxDamage() - 1);
        }
        return new ActionResult<>(EnumActionResult.FAIL, itemstack);
    }

    private ItemStack findAmmo(EntityPlayer player) {
        if (isBattery(player.getHeldItem(EnumHand.OFF_HAND))) {
            return player.getHeldItem(EnumHand.OFF_HAND);
        } else if (isBattery(player.getHeldItem(EnumHand.MAIN_HAND))) {
            return player.getHeldItem(EnumHand.MAIN_HAND);
        } else {
            for (int i = 0; i < player.inventory.getSizeInventory(); ++i) {
                ItemStack itemstack = player.inventory.getStackInSlot(i);

                if (isBattery(itemstack)) {
                    return itemstack;
                }
            }
            return ItemStack.EMPTY;
        }
    }

    private boolean isBattery(ItemStack itemStack) {
        return itemStack.getItem() instanceof Battery && itemStack.getItemDamage() < itemStack.getMaxDamage() - 1;
    }
}
