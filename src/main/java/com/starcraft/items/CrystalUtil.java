package com.starcraft.items;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class CrystalUtil {

    public static ItemStack create(ItemStack stack, CrystalType type) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setString("crystal", type.getName());
        stack.setTagCompound(tag);
        return stack;
    }

    public static String getRegistryNameFromNBT(ItemStack stack) {
        if (stack.hasTagCompound()) {
            if (stack.getTagCompound().hasKey("crystal")) {
                return stack.getTagCompound().getString("crystal");
            }
        }
        return "red";
    }

    public static enum CrystalType {
        RED("red"),
        GREEN("green"),
        BLUE("blue");

        private String name;

        private CrystalType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

}
